import com.epam.training.student_Dimitar_Pavlov.WebDriver.Pages.GoogleCloudCostEstimatePage;
import com.epam.training.student_Dimitar_Pavlov.WebDriver.Pages.GoogleCloudHomepage;
import com.epam.training.student_Dimitar_Pavlov.WebDriver.Pages.GoogleCloudPriceCalculatorPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;

public class Task3GoogleCloudPriceCalculationTest {
    private WebDriver webDriver;
    @Before
    public void setupBrowser(){
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.addArguments("force-device-scale-factor=0.75");
        edgeOptions.addArguments("high-dpi-support=0.75");
        webDriver = new EdgeDriver(edgeOptions);
        webDriver.manage().window().maximize();
    }

    @Test
    public void setCloudComponentsAndSendPriceEstimateOnMailTest(){

        GoogleCloudPriceCalculatorPage priceCalculatorPage = new GoogleCloudHomepage(webDriver)
                .performSearch()
                .selectResult()
                .goToEstimate()
                .setValues()
                .shareEstimate();
        GoogleCloudCostEstimatePage estimatePage = priceCalculatorPage.goToSummary();
        Assert.assertTrue(estimatePage.areValuesCorrect());
/*

        webDriver.get("https://cloud.google.com/products/calculator?hl=pl");
        GoogleCloudCostEstimatePage offTest = new GoogleCloudPriceCalculatorPage(webDriver).goToEstimate()
                .setValues().shareEstimate().goToSummary();
        //GoogleCloudCostEstimatePage estimatePage = offTest.goToSummary();
        Assert.assertTrue(offTest.areValuesCorrect());
*/

    }

    @After
    public void closeBrowser() {
        webDriver.close();
    }
}
