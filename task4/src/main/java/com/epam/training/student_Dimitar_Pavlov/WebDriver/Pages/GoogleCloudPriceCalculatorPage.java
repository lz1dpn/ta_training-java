package com.epam.training.student_Dimitar_Pavlov.WebDriver.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GoogleCloudPriceCalculatorPage {
    private WebDriver webDriver;
    public GoogleCloudPriceCalculatorPage(WebDriver webDriver){
        this.webDriver = webDriver;

    }
    public GoogleCloudPriceCalculatorPage goToEstimate(){
        webDriver.findElement(By.xpath("//div[contains(@class,'Gxwdcd')]/descendant::button")).click();



//      /html/body/c-wiz[1]/div/div/div[1]/div/div/div/div/div/div/div/div[1]/div/div[1]/div/div/div/button
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class,'aHij0b-aGsRMb')]/child::div")));
        webDriver.findElement(By.xpath("//div[contains(@class,'aHij0b-aGsRMb')]/child::div")).click();

        return this;
    }

    public GoogleCloudPriceCalculatorPage setValues(){
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("c11")));
        webDriver.findElement(By.id("c11")).clear();
        webDriver.findElement(By.id("c11")).sendKeys("4");


        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"glue-cookie-notification-bar-1\"]/button")));
        webDriver.findElement(By.xpath("//*[@id=\"glue-cookie-notification-bar-1\"]/button")).click();



        webDriver.findElement(By.xpath("//div[contains(@class,'U4lDT')]/descendant::div[contains(@jsname,'kgDJk')]/descendant::div[contains(@class,'VfPpkd-aPP78e')]")).click();
        webDriver.findElement(By.xpath("//ul[contains(@aria-label, \"Machine type\")]//descendant::li[contains(@data-value, \"n1-standard-8\")]")).click();

        webDriver.findElement(By.xpath("//button[contains(@aria-label,'Add GPUs')]")).click();//add gpu switch


        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(), \"GPU Model\")]/ancestor::div[contains(@class, \"VfPpkd-TkwUic\")]/descendant::div[contains(@class, \"VfPpkd-aPP78e\")]")));
        webDriver.findElement(By.xpath("//span[contains(text(), \"GPU Model\")]/ancestor::div[contains(@class, \"VfPpkd-TkwUic\")]/descendant::div[contains(@class, \"VfPpkd-aPP78e\")]")).click();
        webDriver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[23]/div/div[1]/div/div/div/div[2]/ul/li[1]")).click();

        webDriver.findElement(By.xpath("//span[contains(text(), \"Local SSD\")]/ancestor::div[contains(@class, \"VfPpkd-TkwUic\")]/descendant::div[contains(@class, \"VfPpkd-aPP78e\")]")).click();
        webDriver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[27]/div/div[1]/div/div/div/div[2]/ul/li[3]")).click();


        webDriver.findElement(By.xpath("//label[contains(@for,'1-year')]/parent::div")).click();
        //webDriver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[31]/div/div/div[2]/div/div/div[2]")).click();


        webDriver.findElement(By.xpath("//span[contains(text(), \"Region\")]/ancestor::div[contains(@class, \"VfPpkd-TkwUic\")]/descendant::div[contains(@class, \"VfPpkd-aPP78e\")]")).click();
        WebElement a = webDriver.findElement(By.xpath("//*[@id=\"ow4\"]/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[29]/div/div[1]/div/div/div/div[2]/ul"));
        a.findElement(By.cssSelector("[data-value=\"europe-west4\"]")).click(); //location specified in task was not avalible, so i choose different place from europe


        return this;
    }

    public GoogleCloudPriceCalculatorPage shareEstimate(){

        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.textToBe(By.xpath("//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[1]/div[3]/span[1]"),"$3,882.74"));
        webDriver.findElement(By.xpath("//button[contains(@aria-label,'Open Share Estimate dialog')]")).click();
        return this;
    }

    public String getPriceEstimate(){
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
               .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"yDmH0d\"]/div[5]/div[2]/div/div/div/div[2]/div[1]/div/div/label")));
        String priceEstimate = webDriver.findElement(By.xpath("//*[@id=\"yDmH0d\"]/div[5]/div[2]/div/div/div/div[2]/div[1]/div/div/label")).getText();
        return priceEstimate;
    }

    public GoogleCloudCostEstimatePage goToSummary(){
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
              .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class,'ZgevAb')]")));
        WebElement sumField = webDriver.findElement(By.xpath("//div[contains(@class,'ZgevAb')]"));
        String link = sumField.findElement(By.cssSelector("a")).getAttribute("href");
        webDriver.get(link);
        return new GoogleCloudCostEstimatePage(webDriver);
    }

}
