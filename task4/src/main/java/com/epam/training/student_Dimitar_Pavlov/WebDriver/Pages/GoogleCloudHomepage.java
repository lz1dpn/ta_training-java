package com.epam.training.student_Dimitar_Pavlov.WebDriver.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GoogleCloudHomepage {
    private final String webAdress = "https://cloud.google.com/";
    private WebDriver webDriver;

    By searchBarLocator = By.xpath("//input[contains(@class,'mb2a7b')]");
    public GoogleCloudHomepage(WebDriver webDriver) {
        this.webDriver = webDriver;
        webDriver.get(webAdress);
    }

    public GoogleCloudSearchResults performSearch() {
        new WebDriverWait(webDriver, Duration.ofSeconds(10))
                .until(ExpectedConditions.presenceOfElementLocated(searchBarLocator));
//    //*[@id="kO001e"]/div[2]/div[1]/div/div[2]/div[2]/div[1]/form/div/input

        WebElement searchBar = webDriver.findElement(searchBarLocator);
        searchBar.click();
        searchBar.sendKeys("Google Cloud Platform Pricing Calculator");
        searchBar.sendKeys(Keys.ENTER);
        return new GoogleCloudSearchResults(webDriver);
    }
}
