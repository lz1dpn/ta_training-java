# ta_training-java

## WebDriver Task 1

When performing a task, you must use the capabilities of Selenium WebDriver, a unit test framework (for example JUnit) and the Page Object concept.


Automate the following script:


Open https://pastebin.com/ or a similar service in any browser.
Create 'New Paste' with the following attributes:

* Code: "Hello from WebDriver"
* Paste Expiration: "10 Minutes"
* Paste Name / Title: "helloweb"


