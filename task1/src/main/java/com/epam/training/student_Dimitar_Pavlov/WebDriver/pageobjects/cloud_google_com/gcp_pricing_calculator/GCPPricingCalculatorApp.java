package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.cloud_google_com.gcp_pricing_calculator;

import com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.AbstractPage;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.model.EsimateMailMessageAddressee;

import java.time.Duration;          
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GCPPricingCalculatorApp extends AbstractPage {
  private final Logger log = LogManager.getRootLogger();

  @FindBy(xpath="//md-tab-item/div[@title='GKE Standard Node Pool (Kubernetes Engine)']")
  private WebElement kubernetesEngineSectionDisplayingButton;

  @FindBy(xpath="//md-tab-item/div[@title='Compute Engine']")
  private WebElement computeEngineSectionDisplayingButton;

  /* Check the price is calculated in the right section of the calculator.
     There is a line “Total Estimated Cost: USD ${amount} per 1 month” */
  @FindBy(xpath="//div[@class='cpc-cart-total']//b")
  WebElement totalEstimateTextMessage;

  /* Select 'EMAIL ESTIMATE' */
  @FindBy(xpath="//button[@id='Email Estimate']")
  WebElement emailEstimateFunctionAccessButton;

  /* the email field */
  @FindBy(xpath="//form[@name='emailForm']//input[@type='email']")
  WebElement emailFormEmailField;

  /* Press 'SEND EMAIL'. */
  @FindBy(xpath="//form[@name='emailForm']//button[text()[contains(.,'Send Email')]]")
  WebElement emailFormSendEmailButton;

  public GCPPricingCalculatorApp(WebDriver driver) {
    super(driver);
  }

  public GCPPricingCalculatorAppComputeEnginePanel
    displayComputeEnginePanel(){
  
      new WebDriverWait(driver,
          Duration.ofSeconds(WAIT_TIMEOUT_SECONDS),
          Duration.ofSeconds(POLLING_SECONDS))
          .until(ExpectedConditions.visibilityOfAllElements(
            Arrays.asList(computeEngineSectionDisplayingButton)));

      computeEngineSectionDisplayingButton.click();
      return new GCPPricingCalculatorAppComputeEnginePanel(driver, this);
  }

  public GCPPricingCalculatorEstimateMailMessageBody
    emailTotalEstimate(EsimateMailMessageAddressee addressee){
      WebDriverWait aWait = new WebDriverWait(driver,
          Duration.ofSeconds(WAIT_TIMEOUT_SECONDS),
          Duration.ofSeconds(POLLING_SECONDS+1));

      aWait.until(ExpectedConditions
                  .elementToBeClickable(emailEstimateFunctionAccessButton));
      emailEstimateFunctionAccessButton.click();

      aWait.until(ExpectedConditions
                  .visibilityOf(emailFormSendEmailButton)); // to avoid situation when input focus "jumps"
                                                            // from email to first name field
      aWait.until(ExpectedConditions
                  .visibilityOf(emailFormEmailField));

      emailFormEmailField.sendKeys(addressee.getEmail());
      log.info("«Email Your Estimate» form Email field value: "+emailFormEmailField.getAttribute("value"));

      aWait.until(ExpectedConditions
                  .elementToBeClickable(emailFormSendEmailButton));
      emailFormSendEmailButton.click();

      return new GCPPricingCalculatorEstimateMailMessageBody(driver, addressee);
  }

  public String getTotalEstimate(){
    String totalEstimateValueFound=null; 
    Pattern totalEstimateValueRegex = Pattern.compile("([0-9][0-9.,`'_ ]{2,})\\s");
    Matcher totalEstimateValueMatcher;

    WebDriverWait aWait = new WebDriverWait(driver,
          Duration.ofSeconds(WAIT_TIMEOUT_SECONDS),
          Duration.ofSeconds(POLLING_SECONDS));
    
    aWait.until(ExpectedConditions
                  .elementToBeClickable(totalEstimateTextMessage));

    totalEstimateValueMatcher = totalEstimateValueRegex
                                  .matcher(totalEstimateTextMessage.getText());

    if (totalEstimateValueMatcher.find()){
      totalEstimateValueFound = totalEstimateValueMatcher.group().strip();
    }
    return totalEstimateValueFound;
  }

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }
  
}
