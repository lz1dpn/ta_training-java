package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.yopmail_com;

import com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.AbstractPage;
import com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.cloud_google_com
            .gcp_pricing_calculator.AbstractExpectedEmailMessage;

import java.time.Duration;
import java.util.Arrays;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YopmailComGeneratedMailBoxInboxPage extends AbstractPage {

  private YopmailComInboxMessageEntriesListPane messageEntriesList;
  private final int WAIT_UNTIL_MESSAGES_LIST_IS_DISPLAYED_TIMEOUT = 180; //3 minutes
  private final int MESSAGE_ARRIVAL_WAIT_TIMEOUT = 300; // 5 minutes
  private final int MAILBOX_REFRESH_INTERVAL = 10; // 10 seconds
  private final int MESSAGE_ENTRIES_LIST_FRAME_AVAILABILITY_DELAY_MS=200; // 200 milliseconds

  private final WebDriverWait
    messageEntriesListFrameAvailabilityAfterInboxStateRefreshWait = 
      new WebDriverWait(driver,
        Duration.ofMillis(MESSAGE_ENTRIES_LIST_FRAME_AVAILABILITY_DELAY_MS));

  /* inbox refresh button in the main content document */
  @FindBy(xpath="//button[@id='refresh']")
  WebElement inboxRefreshButton;

  /* frame that contains message entries list */
  @FindBy(xpath="//iframe[@id='ifinbox' and @state='full']")
  WebElement messageEntriesListFrame;

  /* iframe that is visible until inbox is empty */
  @FindBy(xpath="//iframe[@id='ifnoinb' and @state='full']")
  WebElement emptyInboxFrame;

  /* iframe with current message body */
  @FindBy(xpath="//iframe[(@id='ifmobmail' or @id='ifmail') and @state='full']")
  WebElement messageBodyFrame;

  /*  */
  @FindBy(xpath="//div[@class='wminboxheader']/div/button[not(@id)]")
  WebElement inboxMenuButton;

  @FindBy(xpath="//div[@class='menu']//button[@id='delall']")
  WebElement clearInboxButton;

  private class MessagesListIsVisible implements ExpectedCondition<WebElement> {
    private WebElement messagesListElement;
    private WebElement refreshStateElement;
    private WebDriverWait messagesListElementAvailabilityWait;

    public MessagesListIsVisible(WebElement messagesListElement,
                                 WebElement refreshStateElement,
                                 WebDriverWait messagesListElementAvailabilityWait) {
      this.messagesListElement = messagesListElement;
      this.refreshStateElement = refreshStateElement;
      this.messagesListElementAvailabilityWait = messagesListElementAvailabilityWait;
    }
    
    @Override
    public WebElement apply(WebDriver driver) {
      waitForElementToBeClickableAndClickIt(driver, refreshStateElement);

      try {
        messagesListElementAvailabilityWait
            .until(ExpectedConditions.visibilityOf(messagesListElement));
      } catch (TimeoutException | NoSuchElementException e) {
        return null;
      }
      return messagesListElement;

    } 
  }

  private class MessageArrives implements ExpectedCondition<WebElement> {
    private ExpectedCondition<WebElement> messageArrivalConditionToCheck;
    private WebElement refreshStateElement;
    
    public MessageArrives(ExpectedCondition<WebElement> messageArrivalConditionToCheck,
                                 WebElement refreshStateElement) {
      this.messageArrivalConditionToCheck = messageArrivalConditionToCheck;
      this.refreshStateElement = refreshStateElement;
    }
    
    @Override
    public WebElement apply(WebDriver driver) {
      WebElement messageListEntryElement = null;
      waitForElementToBeClickableAndClickIt(driver, refreshStateElement);

      messageListEntryElement = messageArrivalConditionToCheck.apply(driver);

      return messageListEntryElement;
    } 
  }

  public YopmailComGeneratedMailBoxInboxPage(WebDriver driver) {
    super(driver);
  }

  public YopmailComGeneratedMailBoxInboxPage clearInbox(){
    waitForElementToBeClickableAndClickIt(driver, inboxMenuButton);

    waitForPageElementsToBeVisible(Arrays.asList(clearInboxButton));

    if (clearInboxButton.getDomAttribute("disabled") == null) {
      clearInboxButton.click();
      /* https://www.selenium.dev/documentation/webdriver/interactions/alerts/ */
      WebDriverWait aWait = new WebDriverWait(driver, 
                                  Duration.ofSeconds(WAIT_TIMEOUT_SECONDS),
                                  Duration.ofSeconds(POLLING_SECONDS));
      aWait.until(ExpectedConditions.alertIsPresent());
      driver.switchTo().alert().accept();
    }

    return this;
  }

  private WebDriver switchToMessageEntriesListFrame(){
    return driver.switchTo().frame(messageEntriesListFrame);
  }

  private WebDriver switchToMessageBodyFrame(){
    return driver.switchTo().frame(messageBodyFrame);
  }

  private WebDriver switchToDefaultContent(){
    return driver.switchTo().defaultContent();
  }

  public AbstractExpectedEmailMessage 
    waitForEmailMessageArrivalAndReturnIt (AbstractExpectedEmailMessage expectedMessage){

    waitForPageElementsToBeVisible(Arrays.asList(inboxRefreshButton));

    waitUntilMessageEntriesListIsAvailable();

    messageEntriesList = new YopmailComInboxMessageEntriesListPane(driver,
                                messageEntriesListFrameAvailabilityAfterInboxStateRefreshWait);

    WebElement messageListEntry = waitUntilMessageArrives(
      messageEntriesList.getMessageArrivalExpectedCondition(expectedMessage, messageEntriesListFrame));

    switchToMessageEntriesListFrame();
    waitForElementToBeClickableAndClickIt(driver, messageListEntry);

    switchToDefaultContent();
    switchToMessageBodyFrame();
    expectedMessage.doActionsToObtainActualMessageContent();

    return expectedMessage;
  }

  private WebElement waitUntilMessageArrives(ExpectedCondition<WebElement> messageArrivalCondition){
    WebDriverWait aWait = new WebDriverWait(driver,
                                    Duration.ofSeconds(MESSAGE_ARRIVAL_WAIT_TIMEOUT),
                                    Duration.ofSeconds(MAILBOX_REFRESH_INTERVAL));
    return aWait.until(new MessageArrives(messageArrivalCondition, inboxRefreshButton));
  }

  private void waitUntilMessageEntriesListIsAvailable(){
    WebDriverWait aWait = new WebDriverWait(driver,
                                    Duration.ofSeconds(WAIT_UNTIL_MESSAGES_LIST_IS_DISPLAYED_TIMEOUT),
                                    Duration.ofSeconds(MAILBOX_REFRESH_INTERVAL));

    aWait.until(new MessagesListIsVisible(messageEntriesListFrame,
                      inboxRefreshButton,
                      messageEntriesListFrameAvailabilityAfterInboxStateRefreshWait));
  }

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  } 
}
