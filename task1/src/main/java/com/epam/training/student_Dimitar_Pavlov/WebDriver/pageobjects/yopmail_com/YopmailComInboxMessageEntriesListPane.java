package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.yopmail_com;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.AbstractPage;
import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.cloud_google_com
           .gcp_pricing_calculator.AbstractExpectedEmailMessage;

import org.openqa.selenium.By;          
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YopmailComInboxMessageEntriesListPane extends AbstractPage {

  private WebDriverWait messageEntriesFrameAvailabilityAfterInboxStateRefreshWait;

  String messageEntryXPathLocatorTemplate="//div[@class='mctn']/div[@class='m']"+
                "//div[@class='lms' and text()[contains(.,'%s')]]"+
                "/ancestor::div[@class='m']";

  protected YopmailComInboxMessageEntriesListPane(WebDriver driver) {
    super(driver);
  }

  public YopmailComInboxMessageEntriesListPane(WebDriver driver,
           WebDriverWait messageEntriesFrameAvailabilityAfterInboxStateRefreshWait) {
    this(driver);
    this.messageEntriesFrameAvailabilityAfterInboxStateRefreshWait = 
            messageEntriesFrameAvailabilityAfterInboxStateRefreshWait;
  }

  public ExpectedCondition<WebElement>
    getMessageArrivalExpectedCondition(AbstractExpectedEmailMessage expectedMessage,
                                        WebElement messageEntriesFrameElement){
      return new ExpectedCondition<WebElement>() {

        @Override
        public WebElement apply(WebDriver driver) {

          WebElement messageEntryFound=null;

          String messageEntryXPathLocator=
            String.format(messageEntryXPathLocatorTemplate,
              expectedMessage.getExpectedMessageSubject());

          messageEntriesFrameAvailabilityAfterInboxStateRefreshWait.until(
            ExpectedConditions.frameToBeAvailableAndSwitchToIt(messageEntriesFrameElement));

          try {
            messageEntryFound = ExpectedConditions
                                  .presenceOfElementLocated(By.xpath(messageEntryXPathLocator))
                                  .apply(driver);
          } catch (TimeoutException | NoSuchElementException e) {
            driver.switchTo().defaultContent();
            return null;
          }
          driver.switchTo().defaultContent();
          return messageEntryFound;
        }        
      };
  }

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }
  
}
