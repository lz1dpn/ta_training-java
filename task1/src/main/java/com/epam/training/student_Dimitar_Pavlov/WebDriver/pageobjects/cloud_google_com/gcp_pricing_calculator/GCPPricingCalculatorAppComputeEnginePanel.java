package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.cloud_google_com.gcp_pricing_calculator;
          
import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.AbstractPage;

import java.time.Duration;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GCPPricingCalculatorAppComputeEnginePanel extends AbstractPage {
  private GCPPricingCalculatorApp appPageObjectInstance;

  /* Number of instances: 4 */
  @FindBy(xpath="//input[@name='quantity' and @ng-model[contains(.,'computeServer.quantity')]]")
  WebElement numberOfInstancesInput;

  /* What are these instances for? Leave it blank */
  @FindBy(xpath="//label[text()[contains(.,'What are these instances for')]]/following-sibling::input")
  WebElement instancesPurposeInput; 

  /* Operating System / Software: Free: Debian, CentOS, CoreOS, Ubuntu, or another User-Provided OS */
  @FindBy(xpath="//label[text()[contains(.,'Operating System / Software')]]"+
                "/following-sibling::md-select[@role='listbox' and "+
                "@aria-label[contains(.,'Operating System / Software')]]")
  WebElement operatingSystemSelector;

  /* «Operating System / Software:» options list panel */
  @FindBy(xpath="//md-option[@value='free']"+
                "/div[text()[contains(.,'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL')]]"+
                "/ancestor::div[@class[contains(.,'md-select-menu-container')]]")
  WebElement operatingSystemOptionsListPanel; 

  /* «Free: Debian, CentOS, CoreOS, Ubuntu, or another User-Provided OS» option element */
  @FindBy(xpath="//md-option[@value='free']"+
   "/div[text()[contains(.,'Free: Debian, CentOS, CoreOS, Ubuntu or BYOL')]]"+
   "/parent::md-option")
  WebElement operatingSystemOptionFreeOrBYOL;

  /* Provisioning model: Regular */
  @FindBy(xpath="//label[text()[contains(.,'Provisioning model')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'VM Class:')]]")
  WebElement provisioningModelSelector;

  /* «Provisioning model» options list panel */
  @FindBy(xpath = "//md-option[@value='regular']"+
  "/div[text()[contains(.,'Regular')]]"+
  "/ancestor::div[@class[contains(.,'md-select-menu-container')]]")
  WebElement provisioningModelOptionsListPanel;

  @FindBy(xpath="//md-option[@value='regular']"+
                "/div[text()[contains(.,'Regular')]]"+
                "/parent::md-option")
  WebElement provisioningModelOptionRegular;


  /* Machine Family: General purpose */
  @FindBy(xpath="//label[text()[contains(.,'Machine Family')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'Machine Family:')]]")
  WebElement machineFamilySelector;

  @FindBy(xpath="//md-option[@value='gp']/div[text()[contains(.,'General purpose')]]/parent::md-option")
  WebElement machineFamilyOptionGeneralPurpose;

  /* Series: N1 */
  @FindBy(xpath="//label[text()[contains(.,'Series')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'Series:')]]")
  WebElement machineSeriesSelector;

  @FindBy(xpath="//md-option[@value='n1']/div[text()[contains(.,'N1')]]/parent::md-option")
  WebElement machineSeriesOptionN1;

  /* Machine type: n1-standard-8 (vCPUs: 8, RAM: 30 GB) */
  @FindBy(xpath="//label[text()[contains(.,'Machine type')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'Instance type:')]]")
  WebElement machineTypeSelector;

  @FindBy(xpath="//md-option[@value='CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8']"+
  "/div[text()[contains(.,'n1-standard-8 (vCPUs: 8, RAM: 30GB)')]]/parent::md-option")
  WebElement machineTypeOptionN1Standard8;

  /* Select “Add GPUs“ */
  @FindBy(xpath="//form[@name='ComputeEngineForm']"+
                "//div[@class='md-label' and text()[contains(.,'Add GPUs.')]]"+
                "/ancestor::md-checkbox[@aria-label='Add GPUs' and @type='checkbox']")
  WebElement addGPUsCheckbox;

  /* GPU type: NVIDIA Tesla V100 */
  @FindBy(xpath="//label[text()[contains(.,'GPU type')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'GPU type')]]")
  WebElement gpuTypeSelector;

  @FindBy(xpath="//md-option[@value='NVIDIA_TESLA_V100']"+
                "/div[text()[contains(.,'NVIDIA Tesla V100')]]/parent::md-option")
  WebElement gpuTypeOptionNvidiaTeslaN100;

  /* Number of GPUs: 1 */
  @FindBy(xpath="//label[text()[contains(.,'Number of GPUs')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'Number of GPUs:')]]")
  WebElement numberOfGpusSelector;

  @FindBy(xpath="//md-option[@value='1' and @ng-repeat[contains(.,'supportedGpuNumbers')]]"+
                "/div[text()[contains(.,'1')]]/parent::md-option")
  WebElement numberOfGpusOption1Pcs;
  /* Local SSD: 2x375 Gb */
  @FindBy(xpath="//form[@name='ComputeEngineForm']"+
                "//label[text()[contains(.,'Local SSD')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'Local SSD:')]]")
  WebElement localSSDSelector;

  @FindBy(xpath="//md-option[@value='2']/div[text()[contains(.,'2x375 GB')]]/parent::md-option")
  WebElement localSSDOption2Pcs;

  /* Datacenter location: Frankfurt (europe-west3) */
  @FindBy(xpath="//form[@name='ComputeEngineForm']"+
                "//label[text()[contains(.,'Datacenter location')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'Datacenter location:')]]")
  WebElement datacenterLocationSelector;

  @FindBy(xpath="//div[@class[contains(.,'md-select-menu-container')] "+
                "and @aria-hidden='false']"+
                "//md-option[@value='europe-west3']"+
                "/div[text()[contains(.,'Frankfurt (europe-west3)')]]"+
                "/parent::md-option")
  WebElement datacenterLocationOptionEuropeWest3;

  /* Committed usage: 1 Year */
  @FindBy(xpath="//form[@name='ComputeEngineForm']"+
                "//label[text()[contains(.,'Committed usage')]]"+
                "/following-sibling::md-select[@role='listbox' "+
                "and @aria-label[contains(.,'Committed usage:')]]")
  WebElement committedUsageSelector;

  @FindBy(xpath="//div[@class[contains(.,'md-select-menu-container')] "+
                "and @aria-hidden='false']//md-option[@value='1']"+
                "/div[text()[contains(.,'1 Year')]]/parent::md-option")
  WebElement committedUsageOption1Year;

  /* Click 'Add to Estimate' */
  @FindBy(xpath="//form[@name='ComputeEngineForm']"+
                "//button[text()[contains(.,'Add to Estimate')]]")
  WebElement addToEstimateButton;

  public GCPPricingCalculatorAppComputeEnginePanel(WebDriver driver) {
    super(driver);
  }

  public GCPPricingCalculatorAppComputeEnginePanel(WebDriver driver,
           GCPPricingCalculatorApp appPageObjectInstance) {
    this(driver);
    this.appPageObjectInstance = appPageObjectInstance;
  }

  public GCPPricingCalculatorApp
    fillOutComputeEngineCalculationParametersFormAndAddToEstimate() {
      WebDriverWait aWait = new WebDriverWait(driver,
      Duration.ofSeconds(WAIT_TIMEOUT_SECONDS),
      Duration.ofSeconds(POLLING_SECONDS));
      aWait.until(ExpectedConditions.visibilityOfAllElements(
        Arrays.asList(numberOfInstancesInput,
                      operatingSystemSelector,
                      provisioningModelSelector)));
                      /*,
                      machineFamilySelector,
                      machineSeriesSelector,
                      machineTypeSelector,
                      addGPUsCheckbox,
                      gpuTypeSelector,
                      numberOfGpusSelector,
                      localSSDSelector,
                      datacenterLocationSelector,
                      committedUsageSelector,
                      addToEstimateButton */

      numberOfInstancesInput.sendKeys("4");

      operatingSystemSelector.click();
      waitForElementToBeClickableAndClickIt(driver, operatingSystemOptionFreeOrBYOL);

      provisioningModelSelector.click();
      waitForElementToBeClickableAndClickIt(driver, provisioningModelOptionRegular);

      machineFamilySelector.click();
      waitForElementToBeClickableAndClickIt(driver, machineFamilyOptionGeneralPurpose);

      machineSeriesSelector.click();
      waitForElementToBeClickableAndClickIt(driver, machineSeriesOptionN1);

      machineTypeSelector.click();
      waitForElementToBeClickableAndClickIt(driver, machineTypeOptionN1Standard8);

      addGPUsCheckbox.click();

      gpuTypeSelector.click();
      waitForElementToBeClickableAndClickIt(driver, gpuTypeOptionNvidiaTeslaN100);

      numberOfGpusSelector.click();
      waitForElementToBeClickableAndClickIt(driver, numberOfGpusOption1Pcs);

      localSSDSelector.click();
      waitForElementToBeClickableAndClickIt(driver, localSSDOption2Pcs);

      datacenterLocationSelector.click();
      waitForElementToBeClickableAndClickIt(driver,
        datacenterLocationOptionEuropeWest3);

      committedUsageSelector.click();
      waitForElementToBeClickableAndClickIt(driver, committedUsageOption1Year);

      addToEstimateButton.click();

      return this.appPageObjectInstance;               
  }

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }

}
