package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.cloud_google_com.search;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.AbstractPage;
import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.cloud_google_com.SearchResultTransitPage;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GCPSearchResultsPage extends AbstractPage{

  private final String exactlyMatchedSearchResultXPathLocator =
    String.format("%s%s%s%s",
      "//div[@class='gsc-webResult gsc-result']",
      "//*[text()[contains(.,'%s')]]",
      "/ancestor::div[@class='gs-webResult gs-result']",
      "//div[@class='gsc-thumbnail-inside']//a[@class='gs-title']");

  private List<WebElement> exactlyMatchedSearchResults;

  public SearchResultTransitPage 
    clickTheFirstExactlyMatchedSearchResultLink(String searchTerm){
      String exactSearchResultLocator = composeALocatorFromStaticAndDynamicParts(
                                          exactlyMatchedSearchResultXPathLocator,
                                          searchTerm);

      WebDriverWait wait = new WebDriverWait(driver,
                                              Duration.ofSeconds(WAIT_TIMEOUT_SECONDS),
                                              Duration.ofSeconds(POLLING_SECONDS));

      exactlyMatchedSearchResults = wait.until(ExpectedConditions
            .presenceOfAllElementsLocatedBy(By.xpath(exactSearchResultLocator)));

      if (exactlyMatchedSearchResults.size() > 0) {
        exactlyMatchedSearchResults.get(0).click();
      } else {
        throw new UnsupportedOperationException(
                    String.format("%s `%s` %s",
                      "Cannot locate a result with exact match of the search term",
                      searchTerm,
                      "on the search results page"));
      }
      return new SearchResultTransitPage(driver);
    }

  private String composeALocatorFromStaticAndDynamicParts(
    String staticPart, String dynamicPart){
    return String.format(staticPart, dynamicPart);
  }

  public GCPSearchResultsPage(WebDriver driver) {
    super(driver);
  }


  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }
}
