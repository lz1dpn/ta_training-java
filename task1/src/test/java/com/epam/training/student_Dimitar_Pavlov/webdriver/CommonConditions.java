package com.epam.training.student_Dimitar_Pavlov.webdriver;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.core.DriverSingleton;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.core.TestListener;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

@Listeners({TestListener.class})
public abstract class CommonConditions {
  protected WebDriver driver;

  @BeforeMethod()
  public void setUp(){
    driver = DriverSingleton.getDriver();
  }

  @AfterMethod(alwaysRun = true)
  public void stopBrowser(){
    DriverSingleton.closeDriver();
  }
}

