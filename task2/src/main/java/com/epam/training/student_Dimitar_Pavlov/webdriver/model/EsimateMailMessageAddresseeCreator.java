package com.epam.training.student_Dimitar_Pavlov.webdriver.model;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.model.EsimateMailMessageAddressee;

public class EsimateMailMessageAddresseeCreator {

  public static EsimateMailMessageAddressee
    getMinimallySufficientAddressee(String email){
    return new EsimateMailMessageAddressee(email);
  }
}
