package com.epam.training.student_Dimitar_Pavlov.webdriver.pageobjects;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {

  protected WebDriver driver;
  protected abstract AbstractPage openPage();
  protected final int WAIT_TIMEOUT_SECONDS =  15;
  protected final int POLLING_SECONDS =  1;

  protected static Wait<WebDriver> createAFluentWaitInstance(WebDriver driver,
                                                            int waitTimeoutSeconds,
                                                            int pollingEverySeconds){
      return new FluentWait<WebDriver>(driver)
                            .withTimeout(Duration.ofSeconds(waitTimeoutSeconds))
                            .pollingEvery(Duration.ofSeconds(pollingEverySeconds))
                            .ignoring(NoSuchElementException.class)
                            .ignoring(StaleElementReferenceException.class)
                            .withMessage("Timeout for waiting search result list was exceeded!");
    }

  protected WebDriverWait getAWebDriverWait(){
    return new WebDriverWait(driver,
                             Duration.ofSeconds(WAIT_TIMEOUT_SECONDS),
                             Duration.ofSeconds(POLLING_SECONDS));
  }

  protected AbstractPage(WebDriver driver){
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  protected static Function<WebDriver, WebElement>
    getSingleElementWaitingCondition (By elementLocator) {
      return new Function<WebDriver, WebElement>(){
        public WebElement apply(WebDriver driver){
            return driver.findElement(elementLocator);
        } 
      };
    }

  protected static Function<WebDriver, List<WebElement>>
    getElementListWaitingCondition(By elementLocator) {
      return new Function<WebDriver, List<WebElement>>(){
        public List<WebElement> apply(WebDriver driver){
            return driver.findElements(elementLocator);
        } 
      };
    }

  protected void waitForElementToBeClickableAndClickIt(
    WebDriver aDriver, WebElement element){
    WebDriverWait aWait = new WebDriverWait(aDriver,
      Duration.ofSeconds(WAIT_TIMEOUT_SECONDS),
      Duration.ofSeconds(POLLING_SECONDS));
      aWait.until(ExpectedConditions.elementToBeClickable(element));
      element.click();
  }

  protected List<WebElement> fluentlyWaitForElementsList(By elementLocator){
    return createAFluentWaitInstance(driver, WAIT_TIMEOUT_SECONDS, POLLING_SECONDS)
             .until(getElementListWaitingCondition(elementLocator));
  }

  protected WebElement fluentlyWaitForSingleElement(By elementLocator){
    return createAFluentWaitInstance(driver, WAIT_TIMEOUT_SECONDS, POLLING_SECONDS)
             .until(getSingleElementWaitingCondition(elementLocator));
  }

  protected void waitForPageElementsToBeVisible(List<WebElement> webElementsToWaitFor) {
    Wait<WebDriver> fluentWaitInstance=createAFluentWaitInstance(driver,
                         WAIT_TIMEOUT_SECONDS,
                         POLLING_SECONDS);

    fluentWaitInstance.until(ExpectedConditions.visibilityOfAllElements(
        webElementsToWaitFor));
  }  
}
