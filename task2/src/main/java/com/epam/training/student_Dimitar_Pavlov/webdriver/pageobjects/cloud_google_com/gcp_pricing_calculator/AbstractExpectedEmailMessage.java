package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.cloud_google_com.gcp_pricing_calculator;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.model.EsimateMailMessageAddressee;

public interface AbstractExpectedEmailMessage {
  /* Get message expected subject */
  String getExpectedMessageSubject();
  /* get message recipient to which it was actually sent */
  EsimateMailMessageAddressee getActualMailMessageAddressee();
  /* Do some actions to find required content on the message
     web page and save it into pageObject fields */
  AbstractExpectedEmailMessage doActionsToObtainActualMessageContent();
  /* get required message content that was prepared
     using doActionsToObtainActualMessageContent() method */
  String getActualMessageContent();
//  String setExpectedMessageSubject(String expectedSubject);

}
