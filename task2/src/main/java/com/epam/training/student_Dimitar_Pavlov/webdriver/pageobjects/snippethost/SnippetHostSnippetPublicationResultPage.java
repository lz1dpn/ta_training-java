package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.snippethost;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.AbstractPage;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SnippetHostSnippetPublicationResultPage extends AbstractPage{

  @FindBy(xpath="//span[@class='xs-hide' and text()[contains(.,'created:')]]")
  List<WebElement> pasteCreationResultMsgList;

  @FindBy(xpath="//span[@class='xs-hide' and text()[contains(.,'language: bash')]]")
  List<WebElement> publishedContentSyntaxIsBashIndication;

  @FindBy(xpath="//table[@id='snippet-table-interactive']//td[not(@class='line-number')]")
  List<WebElement> publishedContentLinesOfText;

  public SnippetHostSnippetPublicationResultPage (WebDriver driver){
    super(driver);
  }

  public int countTheNumberOfSignsOfSuccessfulPublication(){
    waitForPageElementsToBeVisible(pasteCreationResultMsgList);
    return pasteCreationResultMsgList.size();
  }

  public String getPageTitle(){
    return driver.getTitle();
  }

  public List<String> getPublishedContentLinesOfText(){
    waitForPageElementsToBeVisible(publishedContentLinesOfText);
    List<String> publishedContentLinesOfTextList = new ArrayList<>(){
      {
        for (WebElement anElement : publishedContentLinesOfText){
          add(anElement.getText());
        }
      }
    };
    return publishedContentLinesOfTextList;
  }

  public int getPublishedContentSyntaxIsBashIndication(){
    waitForPageElementsToBeVisible(publishedContentSyntaxIsBashIndication);
    return publishedContentSyntaxIsBashIndication.size();
  } 

  @Override
  protected AbstractPage openPage() {
    throw new UnsupportedOperationException("This method is not intended to be used");
  }
  
}
