package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.snippethost;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.service.TestDataReader;
import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.AbstractPage;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class SnippetHostHomePage extends AbstractPage {
  private static final String ENVDATA_HOME_PAGE_ADDRESS = "sites.snippethost.homepage";
  private final String EXPIRY_IN_10_MINUTES_SELECTOR_VALUE="10min";
  private final String CONTENT_SYNTAX_IS_BASH_SELECTOR_VALUE="bash";

  @FindBy(xpath="//input[@id='snippet-title']")
  WebElement contentTitleInputField;

  @FindBy (xpath = "//textarea[@id='snippet-content']")
  WebElement contentInputArea;

  @FindBy (xpath = "//select[@name='expires']")
  WebElement expirySelector;

  @FindBy (xpath = "//select[@id='snippet-language']")
  WebElement contentSyntaxSelector;

  @FindBy (xpath = "//button[@type='submit']")
  WebElement contentSubmitButton;

  public SnippetHostHomePage(WebDriver driver){
    super(driver);
  }

  public SnippetHostHomePage fillContentTitleField(String aString){
    contentTitleInputField.sendKeys(aString);
    return this;
  }

  public SnippetHostHomePage fillContentArea(String aText){
    contentInputArea.sendKeys(aText);
    return this;
  }

  private void chooseAnOptionByItsValueForASelectorWebElement(
                 WebElement aSelector, String aValue){

    Select selectorInst = new Select(aSelector); 
    selectorInst.selectByValue(aValue);
  }

  public SnippetHostHomePage setContentExpiryTimeoutTo10min(){
    chooseAnOptionByItsValueForASelectorWebElement(
      expirySelector,
      EXPIRY_IN_10_MINUTES_SELECTOR_VALUE);
    return this;
  }

  public SnippetHostHomePage setContentSyntaxTypeToBash(){
    waitForPageElementsToBeVisible(Arrays.asList(contentSyntaxSelector));
    chooseAnOptionByItsValueForASelectorWebElement(
        contentSyntaxSelector,
        CONTENT_SYNTAX_IS_BASH_SELECTOR_VALUE);
    return this;
  }

  public SnippetHostSnippetPublicationResultPage publishASnippet(){
    contentSubmitButton.click();
    return new SnippetHostSnippetPublicationResultPage(driver);
  }

  public SnippetHostSnippetPublicationResultPage
      publishASnippetWithTitleAndExpiryTimeIn10Minutes (
        String title, String content){

    fillContentTitleField(title);
    fillContentArea(content);

    setContentExpiryTimeoutTo10min();

    return publishASnippet();
  }

  public SnippetHostSnippetPublicationResultPage
      publishABashMultilineSnippetWithTitleAndExpiryTimeIn10Minutes (
        String title, List<String> content){

    fillContentTitleField(title);
    fillContentArea(String.join("\n",content));

    setContentExpiryTimeoutTo10min();
    setContentSyntaxTypeToBash();

    return publishASnippet();
  }

  @Override
  public SnippetHostHomePage openPage() {
    driver.get(TestDataReader.getTestData(ENVDATA_HOME_PAGE_ADDRESS));
    waitForPageElementsToBeVisible(Arrays.asList(contentTitleInputField,
                                   contentInputArea,
                                   expirySelector,
                                   contentSubmitButton));
    return this;
  }
}
