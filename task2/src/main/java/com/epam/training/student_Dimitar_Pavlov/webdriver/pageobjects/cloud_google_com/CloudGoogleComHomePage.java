package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.cloud_google_com;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.service.TestDataReader;
import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.AbstractPage;
import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.cloud_google_com
           .search.GCPSearchResultsPage;

import java.util.Arrays;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CloudGoogleComHomePage extends AbstractPage {
  private static final String ENVDATA_HOME_PAGE_ADDRESS = "sites.gcp.homepage";

  @FindBy(xpath="//input[@name='q' and @type='text']")
  private WebElement searchKeywordsInputField;

  public CloudGoogleComHomePage(WebDriver driver) {
    super(driver);
  }

  public GCPSearchResultsPage searchForTerm(String searchTerm){
    searchKeywordsInputField.click();
    searchKeywordsInputField.sendKeys(searchTerm);
    searchKeywordsInputField.sendKeys(Keys.RETURN);
    return new GCPSearchResultsPage(driver);
  }

  @Override
  public CloudGoogleComHomePage openPage() {
    driver.get(TestDataReader.getTestData(ENVDATA_HOME_PAGE_ADDRESS));
    waitForPageElementsToBeVisible(
      Arrays.asList(searchKeywordsInputField));
    return this;
  }
  
}
