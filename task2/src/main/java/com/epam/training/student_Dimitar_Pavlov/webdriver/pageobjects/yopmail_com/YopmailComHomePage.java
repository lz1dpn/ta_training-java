package com.epam.training.student_Dimitar_Pavlov
          .webdriver.pageobjects.yopmail_com;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.AbstractPage;
import com.epam.training.student_Dimitar_Pavlov
		 .webdriver.service.TestDataReader;

import java.util.Arrays;          

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class YopmailComHomePage extends AbstractPage{
  public static final String ENVDATA_HOME_PAGE_ADDRESS="sites.yopmail.homepage";

  @FindBy(xpath="//input[@id='login']")
  WebElement mailBoxAddressInputField;

  @FindBy(xpath="//div[@id='clearbut']/a")
  WebElement clearMailBoxAddressInputFieldButton;

  @FindBy(xpath="//div[@id='refreshbut']/button")
  WebElement checkMailBoxAddressInboxButton;

  @FindBy(xpath="//div[@id='listeliens']//a[@href='email-generator']")
  WebElement randomMailCreationFunctionLink;

  @FindBy(xpath="//ins[@data-adsbygoogle-status and @aria-hidden='false']//iframe")
  WebElement fullPageAdAreaAndFrame;
  
  @FindBy(xpath="//div[@id='dismiss-button']")
  WebElement adDismissButton;

  public YopmailComHomePage(WebDriver driver) {
    super(driver);
  }

  @Override
  public YopmailComHomePage openPage() {
    driver.get(TestDataReader.getTestData(ENVDATA_HOME_PAGE_ADDRESS));
    return this;
  }

  public YopmailComRandomMailBoxAddressGenerationResultPage generateRandomMailBoxAddress(){
    waitForElementToBeClickableAndClickIt(driver, randomMailCreationFunctionLink);
    return new YopmailComRandomMailBoxAddressGenerationResultPage(driver);
  }

  public YopmailComGeneratedMailBoxInboxPage
    openExistingMailBoxInbox(String existingMailBoxAddress){
      waitForPageElementsToBeVisible(
        Arrays.asList(mailBoxAddressInputField,
                      checkMailBoxAddressInboxButton));
      if (! mailBoxAddressInputField.getAttribute("value").isBlank()){
        waitForElementToBeClickableAndClickIt(driver,
          clearMailBoxAddressInputFieldButton);
      }
      mailBoxAddressInputField.sendKeys(existingMailBoxAddress);
      waitForElementToBeClickableAndClickIt(driver,
                                            checkMailBoxAddressInboxButton);

      return new YopmailComGeneratedMailBoxInboxPage(driver);
  }
}
