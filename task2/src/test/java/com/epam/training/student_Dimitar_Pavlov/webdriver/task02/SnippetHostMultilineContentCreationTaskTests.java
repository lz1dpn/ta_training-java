package com.epam.training.student_Dimitar_Pavlov
          .webdriver.task02;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.CommonConditions;

import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.snippethost.SnippetHostHomePage;
import com.epam.training.student_Dimitar_Pavlov
         .webdriver.pageobjects.snippethost
           .SnippetHostSnippetPublicationResultPage;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * WebDriver module task 2 solution implementation
 */
public class SnippetHostMultilineContentCreationTaskTests extends CommonConditions {

    private static final List<String> CONTENT_TO_BE_PUBLISHED = new ArrayList<String>(){
                           {
                             add("git config --global user.name  \"New Sheriff in Town\"");
                             add("git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")");
                             add("git push origin master --force");
                           }};
    private static final String CONTENT_TITLE="how to gain dominance among developers";

    private final Logger log = LogManager.getRootLogger();

    @Test
    public void snippetHostMultilineContentCreationTaskTest()
    {

      SnippetHostSnippetPublicationResultPage
        publicationResultPageInstance  =
            new SnippetHostHomePage(driver)
                  .openPage()
                  .publishABashMultilineSnippetWithTitleAndExpiryTimeIn10Minutes(
                    CONTENT_TITLE,
                    CONTENT_TO_BE_PUBLISHED);

      log.info("Published content page url is as follows: " + driver.getCurrentUrl());

      /* Assert that Browser page title matches 'Paste Name / Title' */
      assertThat("Browser page title doesn't contain initial content title",
                 publicationResultPageInstance.getPageTitle(),
                 containsString(CONTENT_TITLE));

      /* Syntax is suspended for bash */

      assertTrue(publicationResultPageInstance
                    .getPublishedContentSyntaxIsBashIndication() == 1,
                 "The syntax of published content doesn't suspended for bash");

      /* Check that the code matches */
      assertEquals(CONTENT_TO_BE_PUBLISHED.toArray(),
                   publicationResultPageInstance
                     .getPublishedContentLinesOfText()
                     .toArray(),
                  "Published content lines don't match initial content lines or their order");

    }
}
